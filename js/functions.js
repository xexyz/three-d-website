window.onload = function(){
	var canvas = document.getElementById("logocanvas");
	canvas.width  = window.innerWidth;
	canvas.height = window.innerHeight;

	var viewerSettings = {
		cameraEyePosition : [0, -12, 1.0],
		cameraCenterPosition : [0.0, 0.0, 0.0],
		cameraUpVector : [0.0, 0.0, 1.0],
		nearClippingPlane : 1.0,
		farClippingPlane : 100000.0
	};

	var viewer = new JSM.ThreeViewer ();
	if (!viewer.Start (document.getElementById ('logocanvas'), viewerSettings)) {
		viewer = null;
		return;
	}

	var svgObject = document.getElementById ('vg');
	var modelAndMaterials = JSM.SvgToModel (svgObject, 10, 5, null);
	var model = modelAndMaterials[0];
	var materials = modelAndMaterials[1];
	var meshes = JSM.ConvertModelToThreeMeshes (model, materials);
	console.log(meshes);
	
	var pos = getCentroid( meshes[0] )
	//meshes[0].position.set( -pos.x * .1,0,-pos.z * .55 );
	//eshes[1.geometry.applyMatrix(new THREE.Matrix4().makeTranslation( -pos.x, -pos.y, -pos.z ) );	
	var pos2 = getCentroid( meshes[1] )
	//meshes[1].position.set(  pos2 );
	meshes[0].geometry.applyMatrix(new THREE.Matrix4().makeTranslation( -pos2.x, -pos2.y, -pos2.z ) );	
	meshes[1].geometry.applyMatrix(new THREE.Matrix4().makeTranslation( -pos2.x, -pos2.y, -pos2.z ) );	
	viewer.AddMeshes (meshes);
	viewer.renderer.setClearColor( 0x000000 );

	viewer.FitInWindow ();
	viewer.Draw ();
	
	console.log(viewer);
	var SPEED = 0.001;
	var ACCX = 1;
	var LASTX = 1;
	var ACCY = 1;
	var LASTY = 1;
	
	gest.options.subscribeWithCallback(function(gesture) {
		var message = ''; 
		if (gesture.direction) {
			message = gesture.direction;
			if (message == 'Up' || message == 'Long up'){
				ACCX = 1000;
				LASTX = 1;
			}
			else if (message == 'Down' || message == 'Long down'){
				ACCX = 1000;
				LASTX = -1;
			}
			else if (message == 'Right' || message == 'Long right'){
				ACCY = 1000;
				LASTY = 1;
			}
			else if (message == 'Left' || message == 'Long left'){
				ACCY = 1000;
				LASTY = -1;
			}
		} else {
			message = gesture.error.message;
		} 
		console.log(message);
	});
	gest.start();
		
	
	
	Update();
	
	
	function Update ()
	{
		meshes[0].rotation.x -= SPEED * ACCX * 2 * LASTX;
		meshes[0].rotation.y -= SPEED * ACCY * LASTY;
		//meshes[0].rotation.z -= SPEED * 3;
		meshes[1].rotation.x -= SPEED * ACCX * 2 * LASTX;
		meshes[1].rotation.y -= SPEED * ACCY * LASTY;
		//meshes[1].rotation.z -= SPEED * 3;
		viewer.renderer.render (viewer.scene, viewer.camera);
		if (ACCX > 200)
			ACCX -= 5
		else if (ACCX > 1)
			ACCX -= 1;
		
		if (ACCY > 200)
			ACCY -= 5
		else if (ACCY > 1)
			ACCY -= 1;
		
		requestAnimationFrame (Update);

	}
	
	function getCentroid( mesh ) {

		mesh.geometry.computeBoundingBox();
		boundingBox = mesh.geometry.boundingBox;

		var x0 = boundingBox.min.x;
		var x1 = boundingBox.max.x;
		var y0 = boundingBox.min.y;
		var y1 = boundingBox.max.y;
		var z0 = boundingBox.min.z;
		var z1 = boundingBox.max.z;


		var bWidth = ( x0 > x1 ) ? x0 - x1 : x1 - x0;
		var bHeight = ( y0 > y1 ) ? y0 - y1 : y1 - y0;
		var bDepth = ( z0 > z1 ) ? z0 - z1 : z1 - z0;

		var centroidX = x0 + ( bWidth / 2 ) + mesh.position.x;
		var centroidY = y0 + ( bHeight / 2 )+ mesh.position.y;
		var centroidZ = z0 + ( bDepth / 2 ) + mesh.position.z;

		return mesh.geometry.centroid = { x : centroidX, y : centroidY, z : centroidZ };

	}
};
